extends Control

class_name SettingsField

var field_name : Label

var spin_box : SpinBox

var check_box : CheckBox

var type : int

var path : Array

signal number_field_changed

signal bool_field_changed

func _ready():
	field_name = get_node("FieldName")

	spin_box = get_node("Value/SpinBox")
	spin_box.connect("value_changed", self, "on_spin_box_changed")

	check_box = get_node("Value/CheckBox")
	check_box.connect("pressed", self, "on_check_box_changed")

	hide_controls()

func on_spin_box_changed(value):
	emit_signal("number_field_changed", path, value)

func on_check_box_changed():
	emit_signal("bool_field_changed", path, check_box.pressed)

func hide_controls():
	spin_box.hide()
	check_box.hide()

func set_field_path(new_path : Array):
	path = new_path
	field_name.text = path.back()

	name = get_node_name_from_field_path(path)

static func get_node_name_from_field_path(field_path : Array):
	var new_name : String

	for item in field_path:
		new_name += item + "-"

	new_name.erase(len(new_name) - 1, 1)

	return new_name

func setup_spin_box(min_value : float, max_value : float, step : float, default_value : float):
	type = TYPE_REAL

	spin_box.min_value = min_value
	spin_box.max_value = max_value
	spin_box.step = step
	spin_box.value = default_value

	hide_controls()
	spin_box.show()

func setup_check_box(default_value : bool):
	type = TYPE_BOOL

	check_box.pressed = default_value

	hide_controls()
	check_box.show()
