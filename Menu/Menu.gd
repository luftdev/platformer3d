extends Control

class_name Menu

const field_asset_path : String = "Menu/SettingsField.tscn"

const numeric_field_min : float = -1000.0

const numeric_field_max : float = 1000.0

var vbox : VBoxContainer

signal settings_updated

signal copy_pressed

signal paste_pressed

func _ready():
	vbox = get_node("Overlay/List/ScrollContainer/Vbox")

	var copy_button : Button = get_node("Overlay/List/Hbox/CopyButton")
	copy_button.connect("pressed", self, "on_copy_pressed")

	var paste_button : Button = get_node("Overlay/List/Hbox/PasteButton")
	paste_button.connect("pressed", self, "on_paste_pressed")

func on_copy_pressed():
	emit_signal("copy_pressed")

func on_paste_pressed():
	emit_signal("paste_pressed")

func clear():
	for child in vbox.get_children():
		child.queue_free()

func populate_settings(settings : Dictionary, path : Array = []):
	if(not path.empty()):
		create_category_line(path.back())

	for field_name in settings.keys():
		var value = settings.get(field_name)

		var current_path = path + [field_name]

		if(typeof(value) == TYPE_DICTIONARY):
			populate_settings(value, current_path)
		else:
			var field = create_settings_field(current_path) as SettingsField

			if(typeof(value) == TYPE_BOOL):
				field.setup_check_box(value)
			elif(typeof(value) == TYPE_REAL):
				field.setup_spin_box(numeric_field_min, numeric_field_max, 0.1, value)
			elif(typeof(value) == TYPE_INT):
				field.setup_spin_box(numeric_field_min, numeric_field_max, 1, value)
			else:
				printerr("Unknown type for settings field \"%s\"" % field_name)

func create_category_line(category_name : String):
	var category_label = Label.new()
	category_label.text = category_name
	category_label.align = Label.ALIGN_CENTER
	category_label.valign = Label.ALIGN_CENTER
	category_label.rect_min_size.y = 32
	vbox.add_child(category_label)

func create_settings_field(field_path : Array):
	var field = Creator.create(field_asset_path) as SettingsField

	vbox.add_child(field)
	field.connect("number_field_changed", self, "on_number_field_changed")
	field.connect("bool_field_changed", self, "on_bool_field_changed")

	field.set_field_path(field_path)

	return field

func on_number_field_changed(field_path : Array, value : float):
	emit_signal("settings_updated", field_path, value)

func on_bool_field_changed(field_path : Array, value : bool):
	emit_signal("settings_updated", field_path, value)
