extends StateMachine

class_name CameraRigStateMachine

export var automatic_camera_tilt_threshold : float = 0.2

export var automatic_camera_reset_threshold : float = 0.9

var camera_rig : CameraRig

var controller : Controller

var timer : Timer

func _ready():
	camera_rig = get_parent()
	camera_rig.connect("target_acquired", self, "on_target_acquired")
	camera_rig.connect("target_lost", self, "on_target_lost")

	controller = camera_rig.get_node("VirtualController")
	controller.connect("look", self, "on_look")
	controller.connect("move", self, "on_move")

	timer = camera_rig.get_node("Timer")
	timer.connect("timeout", self, "on_timeout")

	add_state("idle", State.new())
	add_state("manual", StateManualCamera.new(camera_rig, controller, timer))
	add_state("strafing", StateStrafingCamera.new(timer))
	add_state("automatic", StateAutomaticCamera.new(camera_rig, controller))
	add_state("targeting", StateTargetingCamera.new(camera_rig, controller, states.manual, timer))
	change_state(states.idle)

func on_target_acquired(target : Target):
	match(current_state):
		states.idle, states.manual, states.strafing, states.automatic:
			if(target):
				change_state(states.targeting)

func on_target_lost():
	match(current_state):
		states.targeting:
			change_state(states.automatic)

func on_timeout():
	match(current_state):
		states.strafing:
			change_state(states.automatic)

func on_look(bearing : Vector2):
	match(current_state):
		states.idle, states.strafing, states.automatic:
			if(bearing != Vector2.ZERO):
				change_state(states.manual)

		states.manual:
			if(bearing == Vector2.ZERO):
				if(controller.move_bearing.y < automatic_camera_tilt_threshold):
					change_state(states.strafing)
				else:
					change_state(states.idle)

func on_move(bearing : Vector2):
	match(current_state):
		states.idle:
			if(bearing.y < automatic_camera_tilt_threshold):
				change_state(states.strafing)

		states.strafing, states.automatic:
			if(bearing == Vector2.ZERO or bearing.y <= -automatic_camera_reset_threshold):
				if(controller.look_bearing == Vector2.ZERO):
					change_state(states.idle)
				else:
					change_state(states.manual)
