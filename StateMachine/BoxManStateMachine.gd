extends StateMachine

class_name BoxManStateMachine

var character : Character

var controller : Controller

var mover : Mover

func _ready():
	character = get_parent()
	character.connect("became_airborne", self, "on_became_airborne")
	character.connect("became_grounded", self, "on_became_grounded")

	controller = character.get_node("VirtualController")
	controller.connect("global_move", self, "on_global_move")
	controller.connect("sprint", self, "on_sprint")
	controller.connect("jump", self, "on_jump")
	controller.connect("lock_on", self, "on_lock_on")

	mover = character.get_node("Mover")

	add_state("standing", State.new())
	add_state("running", StateRunning.new(controller, mover))
	add_state("sprinting", StateSprinting.new(states.running, mover))
	add_state("jumping", StateJumping.new(character, states.running))
	add_state("airborne", StateAirborne.new(character, states.running))
	add_state("landing", State.new())
	add_state("braking", StateBraking.new(mover))
	add_state("dashing", StateDashing.new(controller, mover))
	add_state("leaping", StateLeaping.new(controller, states.jumping, mover))
	change_state(states.standing)

func set_animation_controller(animation_controller : AnimationController):
	animation_controller.connect("animation_finished", self, "on_animation_finished")

func on_global_move(bearing : Vector2):
	match(current_state):
		states.standing:
			if(bearing != Vector2.ZERO):
				change_state(states.running)

		states.running:
			if(bearing == Vector2.ZERO):
				change_state(states.standing)

		states.sprinting:
			if(bearing == Vector2.ZERO):
				change_state(states.braking)

func on_sprint(pressed : bool):
	match(current_state):
		states.running, states.sprinting:
			if(pressed):
				change_state(states.dashing)

func on_jump(pressed : bool):
	match(current_state):
		states.running, states.sprinting, states.standing:
			if(pressed):
				change_state(states.jumping)

		states.dashing:
			if(pressed):
				change_state(states.leaping)

func on_lock_on(pressed : bool):
	if(not pressed):
		return

	var targeter = character.get_node("Targeter")

	if(targeter.has_target()):
		targeter.clear_target()
	else:
		targeter.get_nearest_target()

func on_became_airborne():
	match(current_state):
		states.running, states.sprinting, states.standing, states.braking, states.landing:
			change_state(states.airborne)

func on_became_grounded():
	match(current_state):
		states.airborne, states.jumping, states.leaping:
			if(mover.is_sprinting):
				change_state(states.sprinting)
			else:
				change_state(states.landing)

func on_animation_finished(animation_name : String):
	match(current_state):
		states.jumping:
			change_state(states.airborne)

		states.landing:
			if(mover.is_sprinting):
				change_state(states.sprinting)
			else:
				change_state(states.standing)

		states.braking:
			change_state(states.standing)

		states.dashing:
			change_state(states.sprinting)
