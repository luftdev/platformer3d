extends State

class_name StateRunning

var controller : Controller

var mover : Mover

func _init(controller : Controller, mover : Mover):
	self.controller = controller
	self.mover = mover

func enter():
	mover.set_bearing(controller.global_move_bearing)

func exit():
	mover.set_bearing(Vector2.ZERO)

func physics_process_delegate(delta):
	mover.set_bearing(controller.global_move_bearing)

	if(controller.global_move_bearing == Vector2.ZERO):
		mover.end_sprint()
