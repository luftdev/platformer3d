extends Reference

class_name State

func enter():
	pass

func exit():
	pass

func process_delegate(delta):
	pass

func physics_process_delegate(delta):
	pass
