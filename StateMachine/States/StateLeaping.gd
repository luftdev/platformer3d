extends State

class_name StateLeaping

var controller : Controller

var state_jumping : StateJumping

var mover : Mover

const jump_force : float = 10.0

func _init(controller : Controller, state_jumping : StateJumping, mover : Mover):
	self.controller = controller
	self.state_jumping = state_jumping
	self.mover = mover

func enter():
	state_jumping.perform_jump(jump_force)

	mover.desired_angle = mover.current_angle
	mover.leap_speed()

func exit():
	state_jumping.exit()

