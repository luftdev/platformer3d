extends State

class_name StateDashing

var controller : Controller

var mover : Mover

func _init(controller : Controller, mover : Mover):
	self.controller = controller
	self.mover = mover

func enter():
	var bearing = controller.global_move_bearing.normalized()
	mover.set_bearing(bearing)
	mover.end_sprint()
	mover.start_sprint()
	mover.set_bearing(bearing)
	mover.maximize_speed()
