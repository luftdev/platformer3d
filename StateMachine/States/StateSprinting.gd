extends State

class_name StateSprinting

var state_running : StateRunning

var mover : Mover

func _init(state_running : StateRunning, mover : Mover):
	self.state_running = state_running
	self.mover = mover

func enter():
	state_running.enter()
	mover.start_sprint()

func exit():
	state_running.exit()

func physics_process_delegate(delta):
	mover.set_bearing(state_running.controller.global_move_bearing.normalized())
