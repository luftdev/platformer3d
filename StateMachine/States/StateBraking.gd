extends State

class_name StateBraking

var mover : Mover

func _init(mover : Mover):
	self.mover = mover

func enter():
	mover.end_sprint()
