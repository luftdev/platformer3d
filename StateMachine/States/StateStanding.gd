extends State

class_name StateStanding

var mover : Mover

func _init(character : Character):
	mover = character.get_node("Mover")

func enter():
	mover.end_sprint()
