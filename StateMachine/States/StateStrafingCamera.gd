extends State

class_name StateStrafingCamera

var timer : Timer

func _init(timer : Timer):
	self.timer = timer

func enter():
	timer.start()

func exit():
	timer.stop()
