extends State

class_name StateAutomaticCamera

export var turn_speed_multiplier : float = 0.5

var camera_rig : CameraRig

var controller : Controller

func _init(camera_rig : CameraRig, controller : Controller):
	self.camera_rig = camera_rig
	self.controller = controller

func exit():
	camera_rig.turn(Vector2.ZERO)

func process_delegate(delta):
	var tilt = Vector2.LEFT * sign(controller.move_bearing.x) * turn_speed_multiplier
	camera_rig.turn(tilt * delta)
