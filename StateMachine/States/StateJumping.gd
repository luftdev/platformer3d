extends State

class_name StateJumping

export var jump_force : float = 13

var character : Character

var state_running : StateRunning

func _init(character : Character, state_running : StateRunning):
	self.character = character
	self.state_running = state_running

func enter():
	state_running.enter()
	perform_jump(jump_force)

func perform_jump(jump_force : float):
	character.push(Vector3.UP * jump_force)
	character.set_snap_enabled(false)

func exit():
	state_running.exit()
	character.set_snap_enabled(true)

func physics_process_delegate(delta):
	state_running.physics_process_delegate(delta)
