extends Node

class_name StateMachine 

var current_state : State

var states : Dictionary

var state_names : Dictionary

signal state_changed

func add_state(state_name : String, state : State):
	states[state_name] = state
	state_names[state] = state_name

func get_current_state_name():
	return state_names.get(current_state)

func _process(delta):
	if(current_state):
		current_state.process_delegate(delta)

func _physics_process(delta):
	if(current_state):
		current_state.physics_process_delegate(delta)

func change_state(next_state : State):
	if(not next_state):
		return

	if(current_state == next_state):
		return

	if(current_state):
		current_state.exit()

	current_state = next_state

	current_state.enter()

	emit_signal("state_changed", state_names[current_state])
