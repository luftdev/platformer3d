extends State

class_name StateTargetingCamera

export var vertical_rig_offset : float = -0.40

var state_manual = StateManualCamera

var camera_rig : CameraRig

var controller : Controller

func _init(camera_rig : CameraRig, controller : Controller, state_manual : StateManualCamera, timer : Timer):
	self.state_manual = state_manual
	self.camera_rig = camera_rig
	self.controller = controller

func enter():
	state_manual.enter()

func exit():
	var camera_rig_bearing = camera_rig.get_target_bearing() * Vector3(1, 0, 1)
	camera_rig_bearing = (camera_rig_bearing + Vector3.DOWN * 0.2).normalized()

	camera_rig.set_target_bearing(camera_rig_bearing)

	state_manual.exit()

func process_delegate(delta):
	if(controller.look_bearing == Vector2.ZERO):
		var target_position = camera_rig.target.global_transform.origin
		var camera_rig_bearing = camera_rig.transform.origin.direction_to(target_position) + Vector3.UP *  vertical_rig_offset

		camera_rig.set_target_bearing(camera_rig_bearing)
	else:
		state_manual.process_delegate(delta)
