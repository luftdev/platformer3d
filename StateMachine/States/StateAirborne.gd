extends State

class_name StateAirborne

var state_running : StateRunning

var character : Character

func _init(character : Character, state_running : StateRunning):
	self.character = character
	self.state_running = state_running

func enter():
	state_running.enter()
	character.set_snap_enabled(false)

func exit():
	state_running.exit()
	character.set_snap_enabled(true)

func physics_process_delegate(delta):
	state_running.physics_process_delegate(delta)
