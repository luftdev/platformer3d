extends State

class_name StateManualCamera

var camera_rig : CameraRig

var controller : Controller

var timer : Timer

func _init(camera_rig : CameraRig, controller : Controller, timer : Timer):
	self.camera_rig = camera_rig
	self.controller = controller
	self.timer = timer

func enter():
	timer.stop()

func exit():
	camera_rig.turn(Vector2.ZERO)

func process_delegate(delta):
	var stick = controller.look_bearing
	camera_rig.turn(-stick * delta)
