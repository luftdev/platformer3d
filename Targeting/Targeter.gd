extends Spatial

class_name Targeter

export var reticle_size : float = 4

export var min_size : float = 0.5

export var max_size : float = 4

export var max_target_distance : float = 10

var current_target : Target

var target_reticle : Node2D

signal target_acquired

signal target_lost

func _ready():
	process_priority = 1

func _process(delta):
	update_reticle()

func _physics_process(delta):
	if(current_target):
		var distance = global_transform.origin.distance_to(current_target.global_transform.origin)
		if(distance > max_target_distance):
			clear_target()

func has_target():
	return current_target != null

func get_nearest_target():
	if(current_target):
		return

	var min_distance = INF
	var nearest_target : Target

	for target in get_all_targets():
		var target_origin = target.global_transform.origin
		var distance = global_transform.origin.distance_to(target_origin)

		if(distance < min_distance and distance < max_target_distance):
			min_distance = distance
			nearest_target = target

	if(nearest_target):
		set_target(nearest_target)

func set_target(new_target : Target):
	clear_target()

	current_target = new_target
	target_reticle = ScreenSpace.add_element("Targeting/TargetReticle")

	emit_signal("target_acquired", current_target)

func clear_target():
	if(current_target):
		current_target = null
		emit_signal("target_lost")

	if(target_reticle):
		target_reticle.queue_free()
		target_reticle = null

func get_all_targets():
	return get_tree().get_nodes_in_group(Target.group_name)

func update_reticle():
	if(current_target and target_reticle):
		var target_position = current_target.global_transform.origin

		var camera = get_viewport().get_camera()
		var screen_position = camera.unproject_position(target_position)
		target_reticle.position = screen_position

		var bearing = target_position - camera.global_transform.origin

		target_reticle.visible = bearing.dot(camera.global_transform.basis.z) < 0

		var distance = bearing.length()
		var size = clamp(reticle_size / distance, min_size, max_size)
		target_reticle.scale = Vector2(1, 1) * size
