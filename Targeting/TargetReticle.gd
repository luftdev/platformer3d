extends Node2D

class_name TargetReticle

export var rotation_speed : float = 4

func _process(delta):
	rotate(rotation_speed * delta)
