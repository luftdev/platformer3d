extends Spatial

class_name Target

const group_name : String = "Target"

func _ready():
	add_to_group(group_name)
