extends Node

func add_element(scene_path : String):
	if(not scene_path):
		return

	var packed_scene = load("res://" + scene_path + ".tscn")

	if(not packed_scene):
		return

	var instanced_scene = packed_scene.instance()

	if(not instanced_scene):
		return

	add_child(instanced_scene)

	return instanced_scene
