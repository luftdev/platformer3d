extends Spatial

class_name GameScene

var boxman : BoxMan

var camera_rig : CameraRig

var player_controller : PlayerController

func _ready():
	boxman = get_node("BoxMan")

	camera_rig = get_node("CameraRig") as CameraRig
	camera_rig.set_focus(boxman)

	player_controller = get_node("PlayerController")
	boxman.set_controller(player_controller)
	camera_rig.set_controller(player_controller)

	var targeter = boxman.get_targeter()
	targeter.connect("target_acquired", camera_rig, "set_target")
	targeter.connect("target_lost", camera_rig, "clear_target")

	Settings.connect("settings_updated", self, "on_settings_updated")

	Settings.add_new_category("character", boxman.get_default_settings())
	Settings.add_new_category("camera", camera_rig.get_default_settings())

func on_settings_updated(settings : Dictionary):
	boxman.apply_settings(settings.character)
	camera_rig.apply_settings(settings.camera)
