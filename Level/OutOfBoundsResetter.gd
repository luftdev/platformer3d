extends Area

func _ready():
	connect("body_entered", self, "on_body_entered")

func on_body_entered(body : Spatial):
	body.global_transform.origin = Vector3.ZERO
