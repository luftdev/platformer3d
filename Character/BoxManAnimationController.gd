extends AnimationController

class_name BoxManAnimationController

func construct_animation_map():
	register_animation("standing", "Idle", 0.2)
	register_animation("running", "Run", 0.2)
	register_animation("sprinting", "Sprint", 0.1)
	register_animation("jumping", "Flip", 0)
	register_animation("airborne", "Fall", 0.1)
	register_animation("landing", "Land", 0)
	register_animation("braking", "Brake", 0.1)
	register_animation("dashing", "Dash", 0)
	register_animation("leaping", "FlipLoop", 0)
