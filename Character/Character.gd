extends KinematicBody

class_name Character

export var gravity : float = 50

export var friction : float = 100

export var floor_snap_range : float = 0.5

var virtual_controller : VirtualController

var velocity : Vector3 = Vector3.ZERO

var snap_enabled : int

var is_grounded : bool

var floor_normal : Vector3

signal became_airborne

signal became_grounded

func _ready():
	virtual_controller = get_node("VirtualController")

func _physics_process(delta):
	if(is_grounded):
		push_until(-(velocity * Vector3(1, 0, 1)).normalized() * friction * delta, 0)

	push(Vector3.DOWN * gravity * delta)

	var floor_snap = floor_snap_range * snap_enabled

	velocity = move(velocity, floor_snap)

	var was_grounded = is_grounded
	is_grounded = is_on_floor()

	if(is_grounded and get_slide_count() > 0):
		var collision = get_slide_collision(get_slide_count() - 1)
		floor_normal = collision.normal

	if(was_grounded and not is_grounded):
		emit_signal("became_airborne")
	elif(not was_grounded and is_grounded):
		emit_signal("became_grounded")

func move(offset : Vector3, snap_range : float = 0):
	if(offset == Vector3.ZERO):
		return Vector3.ZERO

	return move_and_slide_with_snap(offset, Vector3.DOWN * snap_range, Vector3.UP, true)

func get_collisions():
	var collisions = []

	for i in range(get_slide_count()):
		collisions.append(get_slide_collision(i))

	return collisions

func push(force : Vector3):
	velocity += force

func push_until(force : Vector3, max_speed : float):
	var bearing = force.normalized()
	var dot = bearing.dot(velocity)

	if(dot < max_speed):
		push(force)
		var dot_after_push = bearing.dot(velocity)
		var overshoot = dot_after_push - max_speed

		if(overshoot > 0):
			push(overshoot * -bearing)

func set_snap_enabled(enabled : bool):
	if(enabled):
		snap_enabled = 1
	else:
		snap_enabled = 0

func set_controller(controller : Controller):
	virtual_controller.set_controller(controller)
