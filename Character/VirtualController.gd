extends Controller

class_name VirtualController

var source_controller : Controller

func set_controller(controller : Controller):
	clear_controller()

	source_controller = controller

	source_controller.connect("move", self, "set_move_bearing")
	source_controller.connect("look", self, "set_look_bearing")
	source_controller.connect("jump", self, "set_jump")
	source_controller.connect("sprint", self, "set_sprint")
	source_controller.connect("lock_on", self, "set_lock_on")

func clear_controller():
	if(not source_controller):
		return

	source_controller.disconnect("move", self, "set_move_bearing")
	source_controller.disconnect("look", self, "set_look_bearing")
	source_controller.disconnect("jump", self, "set_jump")
	source_controller.disconnect("sprint", self, "set_sprint")
	source_controller.disconnect("lock_on", self, "set_lock_on")

	source_controller = null
