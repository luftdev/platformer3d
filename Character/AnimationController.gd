extends Node

class_name AnimationController

var animation_player : AnimationPlayer

var animation_map : Dictionary

var blend_map : Dictionary

signal animation_finished

func _ready():
	var result = get_parent().find_node("AnimationPlayer")
	if(result):
		animation_player = result
		animation_player.connect("animation_finished", self, "on_animation_finished")

	construct_animation_map()

func set_state_machine(state_machine : StateMachine):
	if(state_machine):
		state_machine.connect("state_changed", self, "on_state_changed")
		on_state_changed(state_machine.get_current_state_name())

func register_animation(state_name : String, animation_name : String, blend_time : float):
	animation_map[state_name] = animation_name
	blend_map[animation_name] = blend_time

func construct_animation_map():
	pass

func on_animation_finished(animation_name : String):
	emit_signal("animation_finished", animation_name)

func on_state_changed(state_name : String):
	var animation_name = animation_map.get(state_name)
	if(animation_name):
		animation_player.play(animation_name, blend_map[animation_name])
