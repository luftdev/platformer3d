extends Spatial

class_name Mover

export var run_speed : float = 6

export var sprint_speed : float = 9

export var leap_speed : float = 17

export var walk_speed : float = 4

export var run_force : float = 20

export var running_turn_speed : float = 4 * PI

export var walking_turn_speed : float = 20 * PI

export var turn_acceleration : float = 20 * PI

export var floor_snap_range : float = 0.5

var current_angle : float

var desired_angle : float

var current_turn_speed : float

var current_speed : float

var desired_speed : float

var is_sprinting : bool

var character : Character

signal mover_turned

func _ready():
	character = get_parent()

func _physics_process(delta):
	update_speed(delta)
	var theta = update_angle(delta)

	var bearing = Vector3.RIGHT.rotated(Vector3.UP, -current_angle)

	if(character.is_grounded):
		bearing -= bearing.project(character.floor_normal)
		bearing = bearing.normalized()

	emit_signal("mover_turned", theta, delta)

	if(bearing == Vector3.ZERO):
		return

	character.move(bearing * current_speed, floor_snap_range)

	if(not character.is_grounded):
		return

	for collision in character.get_collisions():
		if(collision.normal.y < 0.75):
			var down_projected = Vector3.DOWN.project(collision.normal)
			var down_slope = (Vector3.DOWN - down_projected).normalized()
			var slide = bearing - bearing.project(collision.normal)
			var correction = -slide.project(down_slope).normalized() * current_speed
			character.move(correction, floor_snap_range)

func update_speed(delta):
	var difference = desired_speed - current_speed
	var force = run_force * delta * sign(difference)
	var new_speed = current_speed + sign(difference) * min(abs(difference), abs(force))
	current_speed = new_speed

func update_angle(delta):
	if(desired_speed == 0):
		return 0

	var max_turn_speed = running_turn_speed
	if(current_speed < walk_speed):
		max_turn_speed = walking_turn_speed

	var adjusted_desired_angle = desired_angle + PI
	var adjusted_current_angle = current_angle + PI

	var diff = adjusted_desired_angle - adjusted_current_angle
	var turn_direction = 1

	if(fposmod(diff, TAU) > PI):
		turn_direction = -1

	current_turn_speed += turn_acceleration * turn_direction * delta
	if(abs(current_turn_speed) > abs(max_turn_speed)):
		current_turn_speed = max_turn_speed * turn_direction

	var theta = current_turn_speed * delta

	if(abs(theta) < abs(diff)):
		adjusted_current_angle += theta
		current_angle = fposmod(adjusted_current_angle, TAU) - PI
		return theta
	else:
		current_angle = desired_angle
		current_turn_speed = 0
		return 0

func set_bearing(new_bearing : Vector2):
	var target_speed = run_speed

	if(is_sprinting):
		target_speed = sprint_speed

	desired_speed = new_bearing.length() * target_speed
	desired_angle = new_bearing.angle()

	if(current_speed == 0 and new_bearing != Vector2.ZERO):
		current_angle = desired_angle

func start_sprint():
	if(not is_sprinting):
		is_sprinting = true
		current_angle = desired_angle

func end_sprint():
	is_sprinting = false

func maximize_speed():
	if(is_sprinting):
		current_speed = sprint_speed
	else:
		current_speed = run_speed

func leap_speed():
	current_speed = leap_speed
	desired_speed = leap_speed
