extends Character

class_name BoxMan

func _ready():
	var animation_controller = get_node("BoxManAnimationController")
	var state_machine = get_node("BoxManStateMachine")

	animation_controller.set_state_machine(state_machine)
	state_machine.set_animation_controller(animation_controller)

func get_targeter():
	return get_node("Targeter")

func apply_settings(settings : Dictionary):
	gravity = settings.gravity
	friction = settings.friction
	floor_snap_range = settings.floor_snap_range

	var mover = get_node("Mover") as Mover

	mover.run_speed = settings.movement.run_speed
	mover.sprint_speed = settings.movement.sprint_speed
	mover.leap_speed = settings.movement.leap_speed
	mover.walk_speed = settings.movement.walk_speed
	mover.run_force = settings.movement.run_force
	mover.running_turn_speed = settings.movement.running_turn_speed
	mover.walking_turn_speed = settings.movement.walking_turn_speed
	mover.turn_acceleration = settings.movement.turn_acceleration
	mover.floor_snap_range = settings.movement.floor_snap_range

	var characterLean = get_node("CharacterLean") as CharacterLean

	characterLean.lean_weight = settings.character_lean.lean_weight
	characterLean.lean_factor = settings.character_lean.lean_factor

func get_default_settings():
	var settings = {}

	settings.gravity = gravity
	settings.friction = friction
	settings.floor_snap_range = floor_snap_range

	var mover = get_node("Mover") as Mover

	settings.movement = {}
	settings.movement.run_speed = mover.run_speed
	settings.movement.sprint_speed = mover.sprint_speed
	settings.movement.leap_speed = mover.leap_speed
	settings.movement.walk_speed = mover.walk_speed
	settings.movement.run_force = mover.run_force
	settings.movement.running_turn_speed = mover.running_turn_speed
	settings.movement.walking_turn_speed = mover.walking_turn_speed
	settings.movement.turn_acceleration = mover.turn_acceleration
	settings.movement.floor_snap_range = mover.floor_snap_range

	var character_lean = get_node("CharacterLean") as CharacterLean

	settings.character_lean = {}
	settings.character_lean.lean_weight = character_lean.lean_weight
	settings.character_lean.lean_factor = character_lean.lean_factor

	return settings
