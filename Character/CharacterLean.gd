extends Node

class_name CharacterLean

export var mesh_name : String

export var lean_weight : float = 6

export var lean_factor : float = 2

var character : Character

var mover : Mover

var mesh : Spatial

var lean : float

func _ready():
	character = get_parent()
	mover = character.get_node("Mover")
	mover.connect("mover_turned", self, "on_mover_turned")

	mesh = character.get_node(mesh_name)

func on_mover_turned(theta : float, delta):
	var lean_speed_multiplier = 1

	if(mover.current_speed <= mover.walk_speed or not character.is_grounded):
		lean_speed_multiplier = 0

	lean = lerp(lean, theta * lean_factor * lean_speed_multiplier, clamp(lean_weight * delta, 0, 1))
	mesh.rotation = Vector3(0, -mover.current_angle + 0.5 * PI, lean)
