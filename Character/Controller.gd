extends Node

class_name Controller

var move_bearing : Vector2

var global_move_bearing : Vector2

var look_bearing : Vector2

var jump_pressed : bool

var sprint_pressed : bool

var lock_on_pressed : bool

signal move

signal global_move

signal look

signal jump

signal sprint

signal lock_on

func set_move_bearing(new_bearing : Vector2):
	move_bearing = new_bearing

	var camera = get_viewport().get_camera()

	if(camera):
		var angle = camera.global_transform.basis.get_euler().y
		global_move_bearing = move_bearing.rotated(-angle)

	emit_signal("move", move_bearing)
	emit_signal("global_move", global_move_bearing)

func set_look_bearing(new_bearing : Vector2):
	look_bearing = new_bearing
	emit_signal("look", look_bearing)

func set_jump(new_pressed : bool):
	jump_pressed = new_pressed
	emit_signal("jump", jump_pressed)

func set_sprint(new_pressed : bool):
	sprint_pressed = new_pressed
	emit_signal("sprint", sprint_pressed)

func set_lock_on(new_pressed : bool):
	lock_on_pressed = new_pressed
	emit_signal("lock_on", lock_on_pressed)
