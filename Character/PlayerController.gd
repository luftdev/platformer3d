extends Controller

class_name PlayerController

export var input_multiplier : float = 1.50

func _process(delta):
	update_left_stick()
	update_right_stick()

	if(Input.is_action_just_pressed("ui_accept")):
		set_jump(true)
	elif(Input.is_action_just_released("ui_accept")):
		set_jump(false)

	if(Input.is_action_just_pressed("sprint")):
		set_sprint(true)
	elif(Input.is_action_just_released("sprint")):
		set_sprint(false)

	if(Input.is_action_just_pressed("lock_on")):
		set_lock_on(true)
	elif(Input.is_action_just_released("lock_on")):
		set_lock_on(false)

func correct_stick(stick : Vector2):
	return (stick * input_multiplier).clamped(1)

func update_left_stick():
	var left_stick : Vector2
	left_stick.x = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left")
	left_stick.y = Input.get_action_strength("ui_down") - Input.get_action_strength("ui_up")
	left_stick = correct_stick(left_stick)

	set_move_bearing(left_stick)

func update_right_stick():
	var right_stick : Vector2
	right_stick.x = Input.get_action_strength("camera_right") - Input.get_action_strength("camera_left")
	right_stick.y = Input.get_action_strength("camera_down") - Input.get_action_strength("camera_up")
	right_stick = correct_stick(right_stick)

	set_look_bearing(right_stick)

