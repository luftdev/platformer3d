extends Spatial

class_name CameraRig

export var turn_speed : float = PI

export var turn_weight : float = 4

export var min_pitch : float = -PI * 0.4

export var max_pitch : float = PI * 0.2

export var follow_weight : float = 6

var spring_arm : SpringArm

var focus : Spatial

var target : Spatial

var target_bearing : Vector3

signal target_acquired

signal target_lost

func _ready():
	spring_arm = get_node("SpringArm")
	target_bearing = global_transform.basis.z

func _process(delta):
	update_rotation(delta)
	update_position(delta)
	untilt()

func set_focus(new_focus : Spatial):
	focus = new_focus

func set_target(new_target : Target):
	target = new_target
	emit_signal("target_acquired", target)

func clear_target():
	target = null
	emit_signal("target_lost")

func update_rotation(delta):
	var pitch = Vector3.DOWN.angle_to(target_bearing) - PI * 0.5
	var clamped = clamp(pitch, min_pitch, max_pitch)
	var correction = clamped - pitch
	var pitch_axis = Vector3.DOWN.cross(target_bearing).normalized()

	target_bearing = target_bearing.rotated(pitch_axis, correction)

	var new_transform = transform.looking_at(transform.origin + target_bearing, Vector3.UP)
	transform = transform.interpolate_with(new_transform, turn_weight * delta)

func untilt():
	global_transform = global_transform.looking_at(global_transform.origin - global_transform.basis.z, Vector3.UP)

func update_position(delta):
	if(focus):
		translation = lerp(translation, focus.global_transform.origin, follow_weight * delta)

func turn(new_turn_direction : Vector2):
	target_bearing = target_bearing.rotated(Vector3.UP, new_turn_direction.x * turn_speed)
	var pitch_axis = target_bearing.cross(Vector3.UP).normalized()

	target_bearing = target_bearing.rotated(pitch_axis, new_turn_direction.y * turn_speed)

func set_target_bearing(new_target_bearing : Vector3):
	target_bearing = new_target_bearing

func get_target_bearing():
	return target_bearing

func set_controller(controller : Controller):
	var virtual_controller = get_node("VirtualController")
	virtual_controller.set_controller(controller)

func get_default_settings():
	var settings = {}

	settings.turn_speed = turn_speed
	settings.turn_weight = turn_weight
	settings.min_pitch = min_pitch
	settings.max_pitch = max_pitch
	settings.follow_weight = follow_weight

	return settings

func apply_settings(settings : Dictionary):
	turn_speed = settings.turn_speed
	turn_weight = settings.turn_weight
	min_pitch = settings.min_pitch
	max_pitch = settings.max_pitch
	follow_weight = settings.follow_weight
