extends Node

const pathPrefix = "res://"

var cache : Dictionary

func create(relativePath : String):
	var path = pathPrefix + relativePath

	if(path in cache.keys()):
		var asset = cache.get(path)
		assert(asset)

		var instance = asset.instance()
		assert(instance)

		return instance
	else:
		var asset = load(path)
		assert(asset)

		cache[path] = asset
		return create(relativePath)
