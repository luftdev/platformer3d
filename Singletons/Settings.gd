extends Node

const menu_asset_path : String = "Menu/Menu.tscn"

var settings : Dictionary

var menu : Menu

signal settings_updated

func add_new_category(category_name : String, category : Dictionary):
	settings[category_name] = category

func _process(delta):
	if(Input.is_action_just_pressed("pause")):
		if(not menu):
			menu = Creator.create(menu_asset_path) as Menu
			add_child(menu)
			menu.connect("settings_updated", self, "on_settings_updated")
			menu.connect("copy_pressed", self, "on_copy_pressed")
			menu.connect("paste_pressed", self, "on_paste_pressed")

			menu.populate_settings(settings)
		else:
			menu.queue_free()
			menu = null

func on_settings_updated(field_path : Array, new_value):
	var category = settings

	for key in field_path.slice(0, field_path.size() - 2):
		category = category[key]

	var field = field_path.back()
	category[field] = new_value

	emit_signal("settings_updated", settings)

func on_copy_pressed():
	OS.clipboard = JSON.print(settings, "  ")

func on_paste_pressed():
	var parsed = JSON.parse(OS.clipboard)

	if(parsed.error != OK):
		return

	settings = parsed.result

	menu.clear()
	menu.populate_settings(settings)
